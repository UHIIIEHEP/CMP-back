
do $$

 declare

     v_record json;

begin


    foreach v_record in array
	
 		array[
			'{"user_id": 1, "profession_id": 4}',
			'{"user_id": 2, "profession_id": 4}',
			'{"user_id": 3, "profession_id": 3}',
			'{"user_id": 4, "profession_id": 3}',
			'{"user_id": 5, "profession_id": 3}',
			'{"user_id": 6, "profession_id": 3}',
			'{"user_id": 7, "profession_id": 2}',
			'{"user_id": 8, "profession_id": 2}',
			'{"user_id": 9, "profession_id": 1}',
			'{"user_id": 10, "profession_id": 1}',
			'{"user_id": 11, "profession_id": 1}',
			'{"user_id": 12, "profession_id": 1}'
		]

    loop
		
		perform pkg_user.user_profession_set(
		  0,
		  (v_record ->> 'user_id')::integer,
		  (v_record ->> 'profession_id')::integer
		);

    end loop;

end;

$$