
do $$

 declare

     v_record json;

begin


    foreach v_record in array
	
 		array[
			'{"name": "АУВ 330 и выше"}',
			'{"name": "АУВ 220 и ниже"}',
			'{"name": "ТТ"}',
			'{"name": "ТН"}',
			'{"name": "ТЗ"}',
			'{"name": "ТЗНП"}',
			'{"name": "ДФЗ"}',
			'{"name": "ПВЗУ"}'
		]

    loop
		
		perform pkg_competent.competent_create(
		  0,
		  v_record ->> 'name'
		);

    end loop;

end;

$$