
do $$

 declare

     v_record json;

begin


    foreach v_record in array
	
 		array[
			'{"user_id": 1, "competent_id": 1}',
			'{"user_id": 1, "competent_id": 2}',
			'{"user_id": 1, "competent_id": 3}',
			'{"user_id": 1, "competent_id": 4}',
			'{"user_id": 1, "competent_id": 5}',
			'{"user_id": 1, "competent_id": 6}',
			'{"user_id": 1, "competent_id": 7}',
			'{"user_id": 1, "competent_id": 8}',

			'{"user_id": 2, "competent_id": 1}',
			'{"user_id": 2, "competent_id": 2}',
			'{"user_id": 2, "competent_id": 3}',
			'{"user_id": 2, "competent_id": 4}',
			'{"user_id": 2, "competent_id": 5}',
			'{"user_id": 2, "competent_id": 6}',

			'{"user_id": 3, "competent_id": 1}',
			'{"user_id": 3, "competent_id": 2}',
			'{"user_id": 3, "competent_id": 3}',
			'{"user_id": 3, "competent_id": 4}',
			'{"user_id": 3, "competent_id": 5}',
			'{"user_id": 3, "competent_id": 6}',

			'{"user_id": 4, "competent_id": 1}',
			'{"user_id": 4, "competent_id": 2}',
			'{"user_id": 4, "competent_id": 3}',
			'{"user_id": 4, "competent_id": 4}',
			'{"user_id": 4, "competent_id": 5}',
			'{"user_id": 4, "competent_id": 6}',

			'{"user_id": 5, "competent_id": 1}',
			'{"user_id": 5, "competent_id": 2}',
			'{"user_id": 5, "competent_id": 5}',
			'{"user_id": 5, "competent_id": 8}',

			'{"user_id": 6, "competent_id": 1}',
			'{"user_id": 6, "competent_id": 2}',
			'{"user_id": 6, "competent_id": 4}',
			'{"user_id": 6, "competent_id": 7}',

			'{"user_id": 7, "competent_id": 7}',
			'{"user_id": 7, "competent_id": 8}',

			'{"user_id": 8, "competent_id": 3}',
			'{"user_id": 8, "competent_id": 4}',

			'{"user_id": 9, "competent_id": 2}',

			'{"user_id": 10, "competent_id": 4}'
		]

    loop
		
		perform pkg_user.user_competent_set(
		  0,
		  (v_record ->> 'user_id')::integer,
		  (v_record ->> 'competent_id')::integer
		);

    end loop;

end;

$$