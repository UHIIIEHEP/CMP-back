
do $$

 declare

     v_record json;

begin


    foreach v_record in array
	
 		array[
			'{"profession_id": 1, "competent_id": 1}',
			'{"profession_id": 1, "competent_id": 2}',

			'{"profession_id": 2, "competent_id": 1}',
			'{"profession_id": 2, "competent_id": 2}',
			'{"profession_id": 2, "competent_id": 3}',
			'{"profession_id": 2, "competent_id": 4}',
            
			'{"profession_id": 3, "competent_id": 1}',
			'{"profession_id": 3, "competent_id": 2}',
			'{"profession_id": 3, "competent_id": 3}',
			'{"profession_id": 3, "competent_id": 4}',
			'{"profession_id": 3, "competent_id": 5}',
			'{"profession_id": 3, "competent_id": 6}',

			'{"profession_id": 4, "competent_id": 1}',
			'{"profession_id": 4, "competent_id": 2}',
			'{"profession_id": 4, "competent_id": 3}',
			'{"profession_id": 4, "competent_id": 4}',
			'{"profession_id": 4, "competent_id": 5}',
			'{"profession_id": 4, "competent_id": 6}',
			'{"profession_id": 4, "competent_id": 7}',
			'{"profession_id": 4, "competent_id": 8}'
		]

    loop
		
		perform pkg_profession.profession_competent_set(
		  0,
		  (v_record ->> 'profession_id')::integer,
		  (v_record ->> 'competent_id')::integer
		);

    end loop;

end;

$$