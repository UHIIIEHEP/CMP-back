
create or replace function pkg_specialty.specialty_create (
  p_user_id integer,
  p_name character varying
)

returns integer

as $$

declare

    v_specialty_id integer;

begin

    insert into public.specialty (
        created_by,
        name
    ) values (
        p_user_id,
        p_name
    )
    returning specialty_id into v_specialty_id;

    return v_specialty_id;

end;

$$

language plpgsql;
