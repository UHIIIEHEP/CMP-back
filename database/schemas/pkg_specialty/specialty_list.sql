create or replace function pkg_specialty.specialty_list (
  p_specialty_id integer[]
)

returns table (
  specialty_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        s.specialty_id,
        s.name
      from
        public.specialty s
      where
        case
          when
            p_specialty_id is not null and
            array_length(p_specialty_id, 1) > 0
              then s.specialty_id = any(p_specialty_id)
              else true
          end;

end;

$$

language plpgsql;
