
create or replace function pkg_session.session_create (
  p_user_id integer,
  p_client character varying,
  p_address character varying
)

returns integer
as $$

declare

  v_session_id integer;

begin

  insert into public.session (
    user_id,
    client,
    address
  ) values (
    p_user_id,
    p_client,
    p_address
  ) 
  returning session_id into v_session_id;

  return v_session_id;


end;

$$

language plpgsql;
