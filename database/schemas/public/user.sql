drop table if exists public.user cascade;

drop sequence if exists public.user_seq;

create sequence if not exists public.user_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.user_seq
  is 'User id sequence';


create table if not exists public.user (
  user_id integer default nextval ('user_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  firstname character varying (100) default null,
  lastname character varying (100) default null,
  patronymic character varying (100) default null,
  login character varying (100) not null unique,
  email character varying (100) not null unique,
  p_hash character varying not null
);

alter table public.user add
  constraint pk_user_id primary key (user_id);