drop table if exists public.device cascade;

drop sequence if exists public.device_seq;

create sequence if not exists public.device_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.device_seq
  is 'device id sequence';


create table if not exists public.device (
  device_id integer default nextval ('device_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying not null unique
);

alter table public.device add
  constraint pk_device_id primary key (device_id);