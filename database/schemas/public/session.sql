drop table if exists public.session cascade;

drop sequence if exists public.session_seq;

create sequence if not exists public.session_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.session_seq
  is 'session id sequence';


create table if not exists public.session (
  session_id integer default nextval ('session_seq'::regclass) not null,
  created timestamp without time zone default now(),
  user_id integer not null,
  client character varying not null,
  address character varying not null
);

alter table public.session add
  constraint pk_session_id primary key (session_id);