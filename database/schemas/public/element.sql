drop table if exists public.element cascade;

drop sequence if exists public.element_seq;

create sequence if not exists public.element_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.element_seq
  is 'element id sequence';


create table if not exists public.element (
  element_id integer default nextval ('element_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying (150) not null unique
);

alter table public.element add
  constraint pk_element_id primary key (element_id);