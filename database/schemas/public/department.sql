drop table if exists public.department cascade;

drop sequence if exists public.department_seq;

create sequence if not exists public.department_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.department_seq
  is 'Department id sequence';


create table if not exists public.department (
  department_id integer default nextval ('department_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying not null
);

alter table public.department add
  constraint pk_department_id primary key (department_id);