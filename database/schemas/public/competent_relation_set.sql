drop table if exists public.relation_competent_set cascade;

drop sequence if exists public.relation_competent_set_seq;

create sequence if not exists public.relation_competent_set_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.relation_competent_set_seq
  is 'relation_competent_set id sequence';


create table if not exists public.relation_competent_set (
  relation_competent_set_id integer default nextval ('relation_competent_set_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  competent_id integer not null,
  relation character varying not null,
  relation_id integer not null
);

alter table public.relation_competent_set add
  constraint pk_relation_competent_set_id primary key (relation_competent_set_id);