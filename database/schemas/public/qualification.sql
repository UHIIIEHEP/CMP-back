drop table if exists public.qualification cascade;

drop sequence if exists public.qualification_seq;

create sequence if not exists public.qualification_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.qualification_seq
  is 'Qualification id sequence';


create table if not exists public.qualification (
  qualification_id integer default nextval ('qualification_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying not null
);

alter table public.qualification add
  constraint pk_qualification_id primary key (qualification_id);