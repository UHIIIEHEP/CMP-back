drop table if exists public.device_element_set cascade;

drop sequence if exists public.device_element_set_seq;

create sequence if not exists public.device_element_set_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.device_element_set_seq
  is 'device_element_set id sequence';


create table if not exists public.device_element_set (
  device_element_set_id integer default nextval ('device_element_set_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  device_id integer not null,
  element_id integer not null
);

alter table public.device_element_set add
  constraint pk_device_element_set_id primary key (device_element_set_id);