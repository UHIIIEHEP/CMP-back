drop table if exists public.user_specialty_relation_set cascade;

drop sequence if exists public.user_specialty_relation_set_seq;

create sequence if not exists public.user_specialty_relation_set_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.user_specialty_relation_set_seq
  is 'user_specialty_relation_set id sequence';


create table if not exists public.user_specialty_relation_set (
  user_specialty_relation_set_id integer default nextval ('user_specialty_relation_set_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  user_id integer unique ,
  relation character varying not null,
  relation_id integer not null

);

alter table public.user_specialty_relation_set add
  constraint pk_user_specialty_relation_set_id primary key (user_specialty_relation_set_id);