drop table if exists public.competent cascade;

drop sequence if exists public.competent_seq;

create sequence if not exists public.competent_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.competent_seq
  is 'competent id sequence';


create table if not exists public.competent (
  competent_id integer default nextval ('competent_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying (150) not null unique
);

alter table public.competent add
  constraint pk_competent_id primary key (competent_id);