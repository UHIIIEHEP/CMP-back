drop table if exists public.specialty cascade;

drop sequence if exists public.specialty_seq;

create sequence if not exists public.specialty_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.specialty_seq
  is 'Specialty id sequence';


create table if not exists public.specialty (
  specialty_id integer default nextval ('specialty_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying not null
);

alter table public.specialty add
  constraint pk_specialty_id primary key (specialty_id);