drop table if exists public.organisation cascade;

drop sequence if exists public.organisation_seq;

create sequence if not exists public.organisation_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.organisation_seq
  is 'Organisation id sequence';


create table if not exists public.organisation (
  organisation_id integer default nextval ('organisation_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  name character varying not null,
  settings jsonb default null
);

alter table public.organisation add
  constraint pk_organisation_id primary key (organisation_id);