drop table if exists public.user_competent_set cascade;

drop sequence if exists public.user_competent_set_seq;

create sequence if not exists public.user_competent_set_seq
  increment 1
  start 1
  minvalue 1
  no maxvalue;

comment on sequence public.user_competent_set_seq
  is 'user_competent_set id sequence';


create table if not exists public.user_competent_set (
  user_competent_set_id integer default nextval ('user_competent_set_seq'::regclass) not null,
  created timestamp without time zone default now(),
  created_by integer not null,
  user_id integer not null,
  competent_id integer default null
);

alter table public.user_competent_set add
  constraint pk_user_competent_set_id primary key (user_competent_set_id);