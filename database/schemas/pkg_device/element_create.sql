
create or replace function pkg_device.element_create (
  p_user_id integer,
  p_name character varying
)

returns integer

as $$

declare

    v_element_id integer;

begin

    insert into public.element (
        created_by,
        name
    ) values (
        p_user_id,
        p_name
    )
    returning element_id into v_element_id;

    return v_element_id;

end;

$$

language plpgsql;
