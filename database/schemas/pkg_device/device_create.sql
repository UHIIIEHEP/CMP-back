
create or replace function pkg_device.device_create (
  p_user_id integer,
  p_name character varying
)

returns integer

as $$

declare

    v_device_id integer;

begin

    insert into public.device (
        created_by,
        name
    ) values (
        p_user_id,
        p_name
    )
    returning device_id into v_device_id;

    return v_device_id;

end;

$$

language plpgsql;
