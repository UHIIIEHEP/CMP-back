create or replace function pkg_device.element_list (
  p_user_id integer,
  p_element_id integer[]
)

returns table
  (
    element_id integer,
    name      character varying
  )

as $$

declare

begin

  return
    query
      select
        e.element_id,
        e.name
      from
        public.element e
      where
        e.element_id = any(p_element_id);

end;

$$

language plpgsql;
