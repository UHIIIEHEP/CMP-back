
create or replace function pkg_device.device_element_set (
  p_user_id integer,
  p_device_id integer,
  p_element_id integer[],
  p_append boolean default true
)

returns integer

as $$

declare

    v_element_id integer;

begin

  if p_append is false then
    delete
      from
        public.device_element_set des
      where
        des.device_id = p_device_id;
  end if;

  foreach v_element_id in array p_element_id
    loop

      if (

        select
          des.element_id
        from
          public.device_element_set des
        where
          des.device_id = p_device_id and
          des.element_id = v_element_id

        ) is null then

          insert into public.device_element_set (
            created_by,
            device_id,
            element_id
          ) values (
            p_user_id,
            p_device_id,
            v_element_id
          );

      end if;

    end loop;

  return p_device_id;

end;

$$

language plpgsql;
