create or replace function pkg_device.device_list (
  p_user_id integer,
  p_device_id integer[]
)

returns table
  (
    device_id integer,
    name      character varying
  )

as $$

declare

begin

  return
    query
      select
        d.device_id,
        d.name
      from
        public.device d
      where
        case
          when p_device_id is not null
            then d.device_id = any(p_device_id)
            else true
          end;

end;

$$

language plpgsql;
