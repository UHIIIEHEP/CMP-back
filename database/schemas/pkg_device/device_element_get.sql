create or replace function pkg_device.device_element_get (
  p_user_id integer,
  p_device_id integer
)

returns integer[]

as $$

declare

  v_element_id  integer[];

begin

  v_element_id = (
    select
      array_agg(des.element_id)
    from
      public.device_element_set des
    where
      des.device_id = p_device_id
  );

  return v_element_id;

end;

$$

language plpgsql;
