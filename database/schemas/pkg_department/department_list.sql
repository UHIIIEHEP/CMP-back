create or replace function pkg_department.department_list (
  p_department_id integer[]
)

returns table (
  department_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        d.department_id,
        d.name
      from
        public.department d
      where
        case
          when
            p_department_id is not null and
            array_length(p_department_id, 1) > 0
              then d.department_id = any(p_department_id)
              else true
          end;

end;

$$

language plpgsql;
