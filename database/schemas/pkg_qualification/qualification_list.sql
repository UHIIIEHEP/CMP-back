create or replace function pkg_qualification.qualification_list (
  p_qualification_id integer[]
)

returns table (
  qualification_id integer,
  name character varying
)

as $$

declare

begin

  return
    query
      select
        q.qualification_id,
        q.name
      from
        public.qualification q
      where
        case
          when
            p_qualification_id is not null and
            array_length(p_qualification_id, 1) > 0
              then q.qualification_id = any(p_qualification_id)
              else true
          end;

end;

$$

language plpgsql;
