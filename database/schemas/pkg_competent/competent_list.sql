
create or replace function pkg_competent.competent_list (
  p_user_id integer,
  p_competent_id integer[] default null
)

returns table (
    competent_id integer,
    name character varying
)

as $$

begin

    return
        query
            select
                cmp.competent_id,
                cmp.name
            from
                public.competent cmp
            where
                case
                    when p_competent_id is not null
                        then cmp.competent_id = any(p_competent_id)
                        else true
                    end;

end;

$$

language plpgsql;
