
create or replace function pkg_competent.relation_competent_get (
  p_user_id integer,
  p_relation character varying,
  p_relation_id integer
)

returns integer[]

as $$

declare

  v_competent_id  integer[];

begin

  v_competent_id := (
    select
      array_agg(rcs.competent_id)
    from
      public.relation_competent_set rcs
    where
      rcs.relation = p_relation and
      rcs.relation_id = p_relation_id
  );

  return v_competent_id;

end;

$$

language plpgsql;