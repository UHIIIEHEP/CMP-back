
create or replace function pkg_competent.relation_competent_set (
  p_user_id integer,
  p_competent_id integer,
  p_relation character varying,
  p_relation_id integer,
  p_append boolean default true
)

returns void

as $$

begin

  if p_append
    then

      insert into public.relation_competent_set (
        created_by,
        competent_id,
        relation,
        relation_id
      ) values (
        p_user_id,
        p_competent_id,
        p_relation,
        p_relation_id
      );

    else

      delete
        from
          public.relation_competent_set rcs
        where
          rcs.competent_id = p_competent_id and
          rcs.relation = p_relation and
          rcs.relation_id = p_relation_id;

  end if;

end;

$$

language plpgsql;