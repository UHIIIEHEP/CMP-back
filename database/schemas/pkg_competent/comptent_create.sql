
create or replace function pkg_competent.competent_create (
  p_user_id integer,
  p_name character varying (150) default null
)

returns integer

as $$

declare

    v_competent_id integer;

begin

    insert into public.competent (
        created_by,
        name
    ) values (
        p_user_id,
        p_name
    )
    returning competent_id into v_competent_id;

    return v_competent_id;

end;

$$

language plpgsql;
