create or replace function pkg_user.user_specialty_relation_set (
  p_created_id integer,
  p_user_id integer,
  p_relation character varying,
  p_relation_id integer
)

returns void

as $$

begin

  insert into public.user_specialty_relation_set (
    created_by,
    user_id,
    relation,
    relation_id
  ) values (
    p_created_id,
    p_user_id,
    p_relation,
    p_relation_id
  );

end;

$$

language plpgsql;

