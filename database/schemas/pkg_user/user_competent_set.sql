
create or replace function pkg_user.user_competent_set (
  p_created_id integer,
  p_user_id integer,
  p_competent_id integer[],
  p_append boolean default true
)

returns integer

as $$

declare

  v_competent_id      integer;

begin

  if p_append is false then
    delete
      from
        public.user_competent_set ucs
      where
        ucs.user_id = p_user_id;
  end if;

  foreach v_competent_id in array p_competent_id
    loop

      if (

        select
          ucs.competent_id
        from
          public.user_competent_set ucs
        where
          ucs.user_id = p_user_id and
          ucs.competent_id = v_competent_id

        ) is null then

          insert into public.user_competent_set (
            created_by,
            user_id,
            competent_id
          ) values (
            p_user_id,
            p_user_id,
            v_competent_id
          );

      end if;

    end loop;

  return p_user_id;

end;

$$

language plpgsql;
