
create or replace function pkg_user.user_info_by_session (
  p_session_id integer
)

returns table (
  user_id integer,
  firstname character varying,
  lastname character varying,
  patronymic character varying,
  email character varying
)

as $$

begin

    return
      query
        select
          u.user_id,
          u.firstname,
          u.lastname,
          u.patronymic,
          u.email
        from
          public.session s
          left join public.user u
            on s.user_id = u.user_id
        where
          s.session_id = p_session_id
        limit 1;

end;

$$

language plpgsql;

