
create or replace function pkg_user.user_competent_get (
  p_user_id integer
)

returns integer[]

as $$

declare

  v_competent_id  integer[];

begin

  v_competent_id := (
    select
      array_agg(ucs.competent_id)
    from
      public.user_competent_set ucs
    where
      ucs.user_id = p_user_id
  );

  return v_competent_id;

end;

$$

language plpgsql;