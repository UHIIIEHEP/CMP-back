
create or replace function pkg_user.user_profession_set (
  p_created_id integer,
  p_user_id integer,
  p_profession_id integer
)

returns void

as $$

begin

    insert into public.user_profession_set (
        created_by,
        user_id,
        profession_id
    ) values (
        p_created_id,
        p_user_id,
        p_profession_id
    );

end;

$$

language plpgsql;
