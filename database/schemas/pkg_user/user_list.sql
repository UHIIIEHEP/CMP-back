create or replace function pkg_user.user_list (
  p_user_id integer[]
)

returns table (
  user_id integer,
  firstname character varying,
  lastname character varying,
  patronymic character varying,
  email character varying
)

as $$

declare

begin

  return
    query
      select
        u.user_id,
        u.firstname,
        u.lastname,
        u.patronymic,
        u.email
      from
        public.user u
      where
        u.user_id = any(p_user_id);

end;

$$

language plpgsql;
