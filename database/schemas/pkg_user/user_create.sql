
create or replace function pkg_user.user_create (
  p_user_id integer,
  p_login character varying,
  p_email character varying,
  p_p_hash character varying,
  p_firstname character varying (100) default null,
  p_lastname character varying (100) default null,
  p_patronymic character varying (100) default null
)

returns integer

as $$

declare

    v_user_id integer;

begin

    insert into public.user (
        created_by,
        firstname,
        lastname,
        patronymic,
        login,
        email,
        p_hash
    ) values (
        p_user_id,
        p_firstname,
        p_lastname,
        p_patronymic,
        p_login,
        p_email,
        p_p_hash
    )
    returning user_id into v_user_id;

    return v_user_id;

end;

$$

language plpgsql;
