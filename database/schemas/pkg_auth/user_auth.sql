
create or replace function pkg_auth.user_auth (
  p_login character varying,
  p_p_hash character varying,
  p_client character varying,
  p_address character varying
)

returns integer

as $$

declare

  v_user_id     integer;
  v_session_id  integer;


begin

  select
    u.user_id
  into
    v_user_id
  from
    public.user u
  where
    p_login = u.login and
    p_p_hash = u.p_hash;

  if v_user_id is null then
    raise 'user_not_found';
  end if;

  select
    s.session_id
  into
    v_session_id
  from
    public.session s
  where
    s.user_id = v_user_id and
    s.client = p_client and
    s.address = p_address;

  if v_session_id is null then

    select * into v_session_id from pkg_session.session_create(v_user_id, p_client, p_address);
    
  end if;

  return v_session_id;

end;

$$

language plpgsql;
