create or replace function pkg_organisation.organisation_list (
  p_organisation_id integer[]
)

returns table (
  organisation_id integer,
  name character varying,
  settings jsonb
)

as $$

declare

begin

  return
    query
      select
        o.organisation_id,
        o.name,
        o.settings
      from
        public.organisation o
      where
        case
          when
            p_organisation_id is not null and
            array_length(p_organisation_id, 1) > 0
              then o.organisation_id = any(p_organisation_id)
              else true
          end;

end;

$$

language plpgsql;
