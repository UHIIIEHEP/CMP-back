
create or replace function pkg_organisation.organisation_create (
  p_user_id integer,
  p_name character varying,
  p_settings json default null
)

returns integer

as $$

declare

    v_organisation_id integer;

begin

    insert into public.organisation (
        created_by,
        name,
        settings
    ) values (
        p_user_id,
        p_name,
        p_settings
    )
    returning organisation_id into v_organisation_id;

    return v_organisation_id;

end;

$$

language plpgsql;
