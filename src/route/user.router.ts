import { Router } from 'express';
import userController from '../controller/user.controller';
import userGuard from '../middleware/userGuard.middleware';
import validator from '../middleware/validator.middleware';

const router = Router();

router.get('/user/list', userController.list);

export default router;
