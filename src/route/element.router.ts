import { Router } from 'express';
import elementController from '../controller/element.controller';
import userGuard from '../middleware/userGuard.middleware';
import validator from '../middleware/validator.middleware';

const router = Router();

router.post('/element/create', userGuard, validator, elementController.create);
router.post('/element/list', userGuard, validator, elementController.list);

export default router;
