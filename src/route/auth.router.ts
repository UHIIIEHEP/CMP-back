import { Router } from 'express';
import authController from '../controller/auth.controller';
import validator from '../middleware/validator.middleware';
import authValidation from '../validation/auth.validation';

const router = Router();

router.post('/login', authValidation.authValidate, validator, authController.login)

export default router;