import { Router } from 'express';
import deviceController from '../controller/device.controller';
import userGuard from '../middleware/userGuard.middleware';
import validator from '../middleware/validator.middleware';

const router = Router();

router.post('/device/create', userGuard, validator, deviceController.create);
router.post('/device/list', userGuard, validator, deviceController.list);
router.post('/device/element/set', userGuard, validator, deviceController.elementSet);
router.post('/device/element/get', userGuard, validator, deviceController.elementGet);

export default router;
