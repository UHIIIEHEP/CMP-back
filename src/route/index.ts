import { Router } from 'express';

import authRouter from './auth.router';
import competentRouter from './competent.router';
import deviceRouter from './device.router';
import elementRouter from './element.router';
import userRouter from './user.router';

require('dotenv').config()

const baseUrl: string = process.env.BASE_URL || '';

const routes = (app: { use: (arg0: string, arg1: Router) => void; })=> {
  app.use(baseUrl, authRouter);
  app.use(baseUrl, competentRouter);
  app.use(baseUrl, deviceRouter);
  app.use(baseUrl, elementRouter);
  app.use(baseUrl, userRouter);
};

export default routes;