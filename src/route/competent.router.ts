import { Router } from 'express';
import competentController from '../controller/competent.controller';
import userGuard from '../middleware/userGuard.middleware';
import validator from '../middleware/validator.middleware';

const router = Router();

router.post('/competent/create', userGuard, validator, competentController.create);
router.post('/competent/list', userGuard, validator, competentController.list);
router.post('/competent/user/set', userGuard, validator, competentController.userSet);
router.post('/competent/user/get', userGuard, validator, competentController.userGet);
router.post('/competent/relation/set', userGuard, validator, competentController.relationSet);
router.post('/competent/relation/get', userGuard, validator, competentController.relationGet);

export default router;
