import {Request, Response} from 'express';
import deviceService from '../service/device.service';

export default {
  create: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const device = await deviceService.create(payload);

      next({device});
    } catch (err) {
      next(err);
    };
  },

  list: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const device = await deviceService.list(payload);

      next({device});
    } catch (err) {
      next(err);
    };
  },

  elementSet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const device = await deviceService.elementSet(payload);

      next({device});
    } catch (err) {
      next(err);
    };
  },

  elementGet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const device = await deviceService.elementGet(payload);

      next({device});
    } catch (err) {
      next(err);
    };
  },
}