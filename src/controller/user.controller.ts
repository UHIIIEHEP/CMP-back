import userService from '../service/user.service';
import {Request, Response} from 'express';

export default {
  list: async (req: Request, res: Response) => {
    try {
      const result = await userService.list();

      // const result = req.body;

      res.send(result);
    } catch (err) {
      throw new Error(err)
    }
  }
}
