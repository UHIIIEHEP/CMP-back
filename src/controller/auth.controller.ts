import {Request, Response} from 'express';
import { validationResult } from 'express-validator/src/validation-result';
import jwt from 'jsonwebtoken';
import authService from '../service/auth.service';

export default {
  login: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    let result;
    try {
      result = await authService.login(payload);
    } catch (err) {
      next(err);
    }

    const session_id = result[0].user_auth;

    const signature: string = process.env.USER_SIGNATURE || '';

    if (!process.env.USER_SIGNATURE) {
      next(new Error ('signature_not_found'));
    };

    var token = jwt.sign({
      session_id,
    }, signature);
  
    next({token});
  },
};
