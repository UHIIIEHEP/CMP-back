import {Request, Response} from 'express';
import elementService from '../service/element.service';

export default {
  create: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const competent = await elementService.create(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },
  
  list: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const competent = await elementService.list(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  }
}