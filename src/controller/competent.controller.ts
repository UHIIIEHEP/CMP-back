import {Request, Response} from 'express';
import competentService from '../service/competent.service';

export default {
  create: async (req: Request, res: Response, next: any) => {
    const payload = req.body;

    try {
      const competent = await competentService.create(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },

  list: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    
    try {
      const competent = await competentService.list(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },

  userSet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    
    try {
      const competent = await competentService.userSet(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },

  userGet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    
    try {
      const competent = await competentService.userGet(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },

  relationSet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    
    try {
      const competent = await competentService.relationSet(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },

  relationGet: async (req: Request, res: Response, next: any) => {
    const payload = req.body;
    
    try {
      const competent = await competentService.relationGet(payload);

      next({competent});
    } catch (err) {
      next(err);
    };
  },
}