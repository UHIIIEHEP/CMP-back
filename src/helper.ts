import { createConnection, getConnectionOptions } from 'typeorm';


const dbQuery = async (pkg: string, foo: string, params: Array<any>) => {

    const connectionOptions = await getConnectionOptions('default');
    const connection = await createConnection(connectionOptions);

    let queryParams = '';

    if (params.length !== 0) {
        params.forEach((elem, index) => {
            let val = '';
            if (Array.isArray(elem)) {
                if (elem.length !== 0) {
                    val = `Array[${elem.join(',')}]`;
                } else {
                    val = 'null';
                }
                
            } else {
                val = typeof(elem) === 'string' ? `'${elem}'`: elem;
            }
            
            queryParams += index !== 0 ? `, ${val}`: `${val}`;
        });
    };
    try {
        const result = await connection.query(`select * from ${pkg}.${foo}(${queryParams})`);
        connection.close();

        return result;
    } catch (err) {
        connection.close();
        throw new Error(err);
    }
}

export {
    dbQuery,
}
