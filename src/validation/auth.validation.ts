const { body } = require('express-validator');


export default {
  authValidate: [
    body('login').isString().isLength({min: 3}),
    body('password').isString().isLength({min: 1})
  ]
}