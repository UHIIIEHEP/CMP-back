import express from 'express';
import routes from './route';
import responseHandler from './middleware/responseHandler.middleware';
import responseLogger from './middleware/responseLogger.middleware';

import authRouter from './route/auth.router'

const bodyParser = require('body-parser');

require('dotenv').config()

const app = express();
const port = Number(process.env.APP_PORT) || 4500;

app.use(bodyParser());

routes(app)
// app.use(router);
// app.use('/v1', authRouter)

app.use(responseLogger);
app.use(responseHandler);

app.listen(port, () => {
  console.log(`Server start ... Port: ${port}`);
})