import { dbQuery } from '../helper';
import { IUserLogin } from '../interface/auth.interface';

export default {
  login: async (payload: IUserLogin) => {
    const result = await dbQuery(
      'pkg_auth',
      'user_auth',
      [
          payload.login,
          payload.password,
          payload.client,
          payload.address,
      ]);

    return result;
  }
}