import { dbQuery } from '../helper';
import {
  IDeviceCreate,
  IDeviceResponse,
  IDeviceList,
  IDeviceElementSet,
  IDeviceElementGet,
} from '../interface/device.interface';

export default {
  create: async(payload: IDeviceCreate): Promise<IDeviceResponse[]> => {
    const {
      userInfoBySession: {user_id},
      name,
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_device',
        'device_create',
        [
          user_id,
          name,
        ]
      );
  
      const device_id = result.device_create;

      return await dbQuery(
        'pkg_device',
        'device_list',
        [
          user_id,
          [device_id],
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  },

  list: async (payload: IDeviceList): Promise<IDeviceResponse[]> => {

    const {
      userInfoBySession: {user_id},
      device_id,
    } = payload;

    try {
      return await dbQuery(
        'pkg_device',
        'device_list',
        [
          user_id,
          device_id,
        ]
      )
    } catch(err) {
      throw new Error(err);
    }
  },

  elementSet: async(payload: IDeviceElementSet): Promise<IDeviceResponse[]> => {
    const {
      userInfoBySession: {user_id},
      device_id,
      element_id,
      append,
    } = payload;

    try {
      await dbQuery(
        'pkg_device',
        'device_element_set',
        [
          user_id,
          device_id,
          element_id,
          append,
        ]
      );

      return await dbQuery(
        'pkg_device',
        'device_list',
        [
          user_id,
          [device_id],
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  },

  elementGet: async(payload: IDeviceElementGet): Promise<IDeviceResponse[]> => {
    const {
      userInfoBySession: {user_id},
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_device',
        'device_element_set',
        [
          user_id,
        ]
      );
  
      const device_id = result.device_create;

      return await dbQuery(
        'pkg_device',
        'device_list',
        [
          user_id,
          [device_id],
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  },
}