import { dbQuery } from '../helper';
import {
  ICompetentCreate,
  ICompetentList,
  ICompetentRelationGet,
  ICompetentRelationResponse,
  ICompetentRelationSet,
  ICompetentResponse,
  ICompetentUserGet,
  ICompetentUserSet,
} from '../interface/competent.interface';
import { IDeviceResponse } from '../interface/device.interface';
import { IElementResponse } from '../interface/element.interface';

export default {
  create: async(payload: ICompetentCreate): Promise<ICompetentResponse[]> => {
    const {
      userInfoBySession: {user_id},
      name,
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_competent',
        'competent_create',
        [
          user_id,
          name,
        ]
      );
  
      const competent_id = result.competent_create;

      return await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          [competent_id],
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  },

  list: async (payload: ICompetentList): Promise<ICompetentResponse[]> => {

    const {
      userInfoBySession: {user_id},
      competent_id,
    } = payload;

    try {
      return await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          competent_id,
        ]
      )
    } catch(err) {
      throw new Error(err);
    }
  },

  userSet: async (payload: ICompetentUserSet): Promise<ICompetentResponse[]> => {

    const {
      userInfoBySession: {user_id: action_user_id},
      user_id,
      competent_id,
      append = true,
    } = payload;

    try {
      await dbQuery(
        'pkg_user',
        'user_competent_set',
        [
          action_user_id,
          user_id,
          competent_id,
          append,
        ]
      )
      return await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          competent_id,
        ]
      )
    } catch(err) {
      throw new Error(err);
    }
  },

  userGet: async (payload: ICompetentUserGet): Promise<ICompetentResponse[]> => {

    const {
      userInfoBySession: {user_id},
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_user',
        'user_competent_get',
        [
          user_id,
        ]
      );

      const competent_id = result.user_competent_get;

      return await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          competent_id,
        ]
      )
    } catch(err) {
      throw new Error(err);
    };
  },

  relationSet: async (payload: ICompetentRelationSet): Promise<ICompetentRelationResponse> => {

    const {
      userInfoBySession: {user_id},
      competent_id,
      relation_type,
      relation_id,
      append = true,
    } = payload;

    try {

      await dbQuery(
        'pkg_competent',
        'relation_competent_set',
        [
          user_id,
          competent_id,
          relation_type,
          relation_id,
          append,
        ]
      );

      const [result] = await dbQuery(
        'pkg_competent',
        'relation_competent_get',
        [
          user_id,
          relation_type,
          relation_id,
        ]
      );
  
      const competent_id_get = result.relation_competent_get;

      const competent = await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          competent_id_get,
        ]
      );

      let relation: IDeviceResponse | IElementResponse;

      switch (relation_type) {
        case 'device':
          relation = await dbQuery(
            'pkg_device',
            'device_list',
            [
              user_id,
              [relation_id],
            ],
          );
          break
        case 'element':
          relation = await dbQuery(
            'pkg_device',
            'element_list',
            [
              user_id,
              [relation_id],
            ],
          );
          break;

        default:
          throw new Error ('Bad relaion type');
      };

      return {
        competent,
        relation,
      };

    } catch(err) {
      throw new Error(err);
    }
  },

  relationGet: async (payload: ICompetentRelationGet): Promise<ICompetentRelationResponse> => {

    const {
      userInfoBySession: {user_id},
      relation_type,
      relation_id,
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_competent',
        'relation_competent_get',
        [
          user_id,
          relation_type,
          relation_id,
        ],
      );
  
      const competent_id_get = result.relation_competent_get;

      const competent = await dbQuery(
        'pkg_competent',
        'competent_list',
        [
          user_id,
          competent_id_get,
        ],
      );

      let relation: IDeviceResponse | IElementResponse;

      switch (relation_type) {
        case 'device':
          relation = await dbQuery(
            'pkg_device',
            'device_list',
            [
              user_id,
              [relation_id],
            ],
          );
          break
        case 'element':
          relation = await dbQuery(
            'pkg_device',
            'element_list',
            [
              user_id,
              [relation_id],
            ],
          );
          break;

        default:
          throw new Error ('Bad relaion type');
      };

      return {
        competent,
        relation,
      };
    } catch(err) {
      throw new Error(err);
    }
  },
}
