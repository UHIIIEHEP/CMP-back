import { dbQuery } from '../helper';
import {
  IElementCreate,
  IElementList,
  IElementResponse,
} from '../interface/element.interface';

export default {
  create: async (payload: IElementCreate): Promise<IElementResponse[]> => {
    const {
      userInfoBySession: {user_id},
      name,
    } = payload;

    try {
      const [result] = await dbQuery(
        'pkg_device',
        'element_create',
        [
          user_id,
          name,
        ]
      );
  
      const element_id = result.element_create;

      return await dbQuery(
        'pkg_device',
        'element_list',
        [
          user_id,
          [element_id],
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  },

  list: async (payload: IElementList): Promise<IElementResponse[]> => {
    const {
      userInfoBySession: {user_id},
      element_id,
    } = payload;

    try {
      return await dbQuery(
        'pkg_device',
        'element_list',
        [
          user_id,
          element_id,
        ]
      );
    } catch(err) {
      throw new Error(err);
    }
  }
}