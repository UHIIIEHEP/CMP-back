export interface IUserInfo {
  user_id: number,
  firstname: string,
  lastname: string,
  patronymic: string,
  email: string,
}