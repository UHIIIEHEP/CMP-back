import { IDeviceResponse } from "./device.interface";
import { IElementResponse } from "./element.interface";
import { IUserInfo } from "./user.interface";

export interface ICompetent {
  name: string,
}

export interface ICompetentCreate extends ICompetent {
  userInfoBySession: IUserInfo,
};

export interface ICompetentList {
  competent_id?: number[],
  userInfoBySession: IUserInfo,
};

export interface ICompetentResponse {
  competent_id: number,
  name: string,
}

export interface ICompetentUserSet {
  action_user_id: number,
  user_id: number,
  competent_id?: number[],
  append?: boolean,
  userInfoBySession: IUserInfo,
};

export interface ICompetentUserGet {
  userInfoBySession: IUserInfo,
};

export interface ICompetentRelationSet {
  relation_type: string,
  relation_id: number,
  competent_id: number,
  append?: boolean,
  userInfoBySession: IUserInfo,
};

export interface ICompetentRelationGet {
  relation_type: string,
  relation_id: number,
  userInfoBySession: IUserInfo,
};

export interface ICompetentRelationResponse {
  competent: ICompetentResponse,
  relation: IDeviceResponse | IElementResponse,
}
