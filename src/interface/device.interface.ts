import { IUserInfo } from "./user.interface";

export interface IDevice {
  name: string,
};

export interface IDeviceCreate extends IDevice {
  userInfoBySession: IUserInfo,
};

export interface IDeviceList {
  device_id?: number[],
  userInfoBySession: IUserInfo,
};

export interface IDeviceResponse {
  device_id: number,
  name: string,
};

export interface IDeviceElementSet {
  action_user_id: number,
  device_id: number,
  element_id?: number[],
  append?: boolean,
  userInfoBySession: IUserInfo,
};

export interface IDeviceElementGet {
  userInfoBySession: IUserInfo,
};
