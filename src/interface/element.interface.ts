import { IUserInfo } from "./user.interface";

export interface IElement {
  name: string,
}

export interface IElementCreate extends IElement {
  userInfoBySession: IUserInfo,
};

export interface IElementList {
  element_id?: number[],
  userInfoBySession: IUserInfo,
}

export interface IElementResponse {
  element_id: number,
  name: string,
}