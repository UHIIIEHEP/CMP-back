export interface IUserLogin {
  login: string,
  password: string,
  client: string,
  address: string,
};
