import {Request, Response} from 'express';
import jwt from 'jsonwebtoken';
import { dbQuery } from '../helper';
import { IUserInfo } from '../interface/user.interface';

require('dotenv').config();

const userGuard = async (req: Request, res: Response, next: any) => {
  let session_id: number;

  if (!req.headers['authorization']) {
    throw new Error ('no_auth');
  };

  const token = req.headers['authorization'].split(' ');

  if (token[0].toLocaleLowerCase() !== 'bearer') {
    throw new Error ('bad_token');
  };

  try{
    const decode: any= jwt.decode(token[1]);
    session_id = decode.session_id;
  } catch(err) {
    throw new Error (err)
  };

  try {
    const [userInfoBySession]: IUserInfo[] = await dbQuery(
      'pkg_user',
      'user_info_by_session',
      [
        session_id,
      ]
    );

    Object.assign(req.body, {userInfoBySession});

  } catch(err) {
    throw new Error (err)
  };
  
  next();
};

export default userGuard;
