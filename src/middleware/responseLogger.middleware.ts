import {Request, Response} from 'express';

const responseLogger = (msg: any, req: Request, res: Response, next: any) => {
  next(msg);
};

export default responseLogger;
