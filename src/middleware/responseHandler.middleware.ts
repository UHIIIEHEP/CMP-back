import {Request, Response} from 'express';

const responseHandler = (msg: any, req: Request, res: Response, next: any) => {
  let status;
  let message;

  console.log({
    message: msg.message,
    // msg,
  })

  if (!msg.message) {
    status = 200;
    message = {
      body: msg,
    };
  } else {
    message = msg.message;
    switch(msg.message) {
      case('user_not_found'):
        status = 404;
        break;
  
      default:
        status = 500;
    };
  };

  res.status(status).send(message);
};

export default responseHandler;
